# Setup for plotting
source("circ_setup.r")
# Subsetting and organizing data
source("circ_arrange.r")

# 11 December 2017
# Jennifer L Hu
# plot_classed.r
#
# Plots data from particles/XXX.csv.
#

plot_data <- sum_data
bar_style <- "fill"
y_label <- "Count"
if (bar_style == "fill") {
	y_label <- "Fraction"
}
if (!no_timepoints) {
	if (iind > 0) {
	  aes_x <- indvar_to_plot
	  x_label <- indvar_to_plot
	}
	else {
		aes_x <- 'timepoint'
		if (combine_day) {
		  x_label <- "Time (days)"
		} else
		  x_label <- "Time (hr)"
	}
} else {
  aes_x <- 'strain'
  x_label <- "Specimen ID"
}

if (plot_by_expt) {
	g <- ggplot(plot_data, aes_string(aes_x, 'count', fill = 'outcome'))
	# in a row
	# g <- g + facet_grid(. ~ strain, scales = "free", space = "free_x")
	# in even rows
	if (length(dates) < length(strains)) {
		g <- g + facet_grid(date ~ strain, scales = "free_x", space = "free_x")
	} else {
		g <- g + facet_grid(strain ~ date, scales = "free_x", space = "free_x")
	}
} else if (all(include_mixed_strains==c(FALSE,TRUE,FALSE))) {
  # use LEP strain as x axis
  g <- ggplot(plot_data, aes_string('strain', 'count', fill = 'outcome'))
  g <- g + facet_grid(strainL ~ ., scales = "free_y", space = "free_x")
  x_label <- "LEP Specimen IDs"
} else if (all(include_mixed_strains==c(FALSE,FALSE,TRUE))) {
  # use MEP strain as x axis
  g <- ggplot(plot_data, aes_string('strain', 'count', fill = 'outcome'))
  g <- g + facet_grid(strainM ~ ., scales = "free_y", space = "free_x")
  x_label <- "LEP Specimen IDs"
} else {
	# normal plot
	g <- ggplot(plot_data, aes_string(aes_x, 'count', fill = 'outcome'))
	g <- g + facet_grid(. ~ strain, scales = "free_y", space = "free_x")
}
# adjust look and labels
g <- g + labs(x = x_label, y = y_label)
g <- g + theme(text = element_text(size = 20), axis.text.x=element_text(angle=60, hjust=1))
g <- g + scale_fill_manual(values = outcome_colors, name = "outcome", labels = outcome_labels)
g <- g + geom_bar(stat = "identity", position=bar_style)
print(g)

if (do_statistics && combine_day) {
	# collapse data into success (correct + mostly) and failure (everything else)
	success_data <- sum_data[(sum_data$outcome == "correct") | (sum_data$outcome == "mostly"),]
	failure_data <- sum_data[!(sum_data$outcome == "correct") & !(sum_data$outcome == "mostly"),]
	if (stratify_by_expt) {
		success_data <- aggregate(success_data$count, FUN = sum, 
			by = list(timepoint = success_data$timepoint, strain=success_data$strain, 
				condition=success_data$condition, date=success_data$date))
		failure_data <- aggregate(failure_data$count, FUN = sum,
			by = list(timepoint = failure_data$timepoint, strain=failure_data$strain, 
				condition=failure_data$condition, date=failure_data$date))
		# change column name from x to success/failure
		names(success_data)[names(success_data) == "x"] <- "success"
		names(failure_data)[names(failure_data) == "x"] <- "failure"
		binom_data <- merge(success_data, failure_data, 
			by=c("timepoint", "strain", "condition", "date"))
	} else {
		# summed by timepoint, strain, and condition
		success_data <- aggregate(success_data$count, FUN = sum, 
			by = list(timepoint = success_data$timepoint, strain=success_data$strain, 
				condition=success_data$condition))
		failure_data <- aggregate(failure_data$count, FUN = sum,
			by = list(timepoint = failure_data$timepoint, strain=failure_data$strain, 
				condition=failure_data$condition))
		# change column name from x to success/failure
		names(success_data)[names(success_data) == "x"] <- "success"
		names(failure_data)[names(failure_data) == "x"] <- "failure"
		binom_data <- merge(success_data, failure_data, by=c("timepoint", "strain", "condition"))
	}
	rm(success_data, failure_data)
	binom_data$total <- binom_data$success + binom_data$failure
	binom_data$proportion <- binom_data$success / binom_data$total

	plot_list <- lapply(unique(binom_data$timepoint), stats_comparisons)
	multiplot(plotlist=plot_list, cols=2)
}