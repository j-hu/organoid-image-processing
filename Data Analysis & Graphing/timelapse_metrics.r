source('constants.r')

file.rawdata <- file.path(hddir, 'timelapse', '2019-11-19', 'organoid_data.csv')
file.data <- file.path(hddir, 'timelapse', '2019-11-19', 'organoid_data.rds')

if (!(file.exists(file.data))) {
  data <- read.csv(file.rawdata, stringsAsFactors = FALSE)
  
  # this is a really slow and horrible way to do this
  metadata <- rbind_list(lapply(data$Filename, FUN = function(x) {
    interval = as.numeric(substring(x, regexpr('Interval', x) + str_length('Interval'), regexpr(' P', x)-1))
    frame = as.numeric(substring(x, regexpr('Segmentation_', x) + str_length('Segmentation_'), regexpr('.tiff', x)-1))
    if (interval == 0) {
      start_time = 1.5*60
      timestep = 23
    } else if (interval == 1) {
      start_time = 15*60
      timestep = 24
    } else if (interval == 2) {
      start_time = 22*60
      timestep = 24
    }
    well = as.character(substring(x, regexpr('-', x) + str_length('-'), regexpr(' ntps', x)-1))
    condition = NA
    virus <- NA
    drug <- NA
    if (well == 'A01') {
      condition <- '240L GFP_DMSO'
      virus <- 'GFP'
      drug <- 'DMSO'
    } else if (well == 'A02') {
      condition <- '240L GFP_copanlisib'
      virus <- 'GFP'
      drug <- 'copanlisib'
    } else if (well == 'A03') {
      condition <- '240L GFP_MK2206'
      virus <- 'GFP'
      drug <- 'MK2206'
    } else if (well == 'A04') {
      condition <- '240L GFP_ITSX'
      virus <- 'GFP'
      drug <- 'ITSX'
    } else if (well == 'B01') {
      condition <- '240L H1047R_DMSO'
      virus <- 'H1047R'
      drug <- 'DMSO'
    } else if (well == 'B02') {
      condition <- '240L H1047R_copanlisib'
      virus <- 'H1047R'
      drug <- 'copanlisib'
    } else if (well == 'B03') {
      condition <- '240L H1047R_MK2206'
      virus <- 'H1047R'
      drug <- 'MK2206'
    } else if (well == 'B04') {
      condition <- '240L H1047R_ITSX'
      virus <- 'H1047R'
      drug <- 'ITSX'
    } 
    
    return( data.frame(Interval = interval,
                       Position = as.numeric(substring(x, regexpr(' P', x) + str_length(' P'), regexpr('-', x)-1)),
                       Well = well, Condition = condition, Virus = virus, Drug = drug,
                       Frame = frame, Hours = (start_time + timestep*frame)/60) )
  }))
  
  data <- cbind(data, metadata)
  rm(metadata)

  saveRDS(data, file.data)
} else { data <- readRDS(file.data) }

# remove NA
data <- data[!(is.na(data$Ratio)) & (data$Ratio > 0),]


# try to plot
p <- ggplot(data[data$Interval > 0 & data$Drug == 'DMSO' & data$Frame < 42, ], 
            aes(x = Inner_LEP_Enrichment + 0.001, y = Intercentroid + 0.001,
                color = Virus, group = Position)) + 
  geom_path(size = 1.2, alpha = 0.8) + theme(legend.position = 'right') +
  scale_color_manual(values = c('darkgreen', 'red3'), name = 'Transgene') +
  labs(x = 'Inner LEP Enrichment', y = 'Intercentroid Distance', 
       title = 'Organoid Structures during Timelapse')
print(p)
p_anim <- p + transition_reveal(Hours)
animate(p_anim, nframes = 200, fps = 7, start_pause = 5, end_pause = 5)
anim_save(file.path(hddir, 'timelapse', '2019-11-19', 'timelapse_anim_path.gif'),
          animation = p_anim)

p <- ggplot(data[data$Interval > 0 & data$Drug == 'DMSO' & data$Frame < 42, ], 
            aes(x = Inner_LEP_Enrichment + 0.001, y = Intercentroid + 0.001,
                color = Virus, group = Position)) + 
  geom_point(size = 8, alpha = 0.8) + theme(legend.position = 'right') +
  scale_color_manual(values = c('darkgreen', 'red3'), name = 'Transgene') +
  labs(x = 'Inner LEP Enrichment', y = 'Intercentroid Distance', 
       title = 'Organoid Structures during Timelapse')
p_anim <- p + transition_time(Hours) + labs(title = 'Hours: {frame_time}')
animate(p_anim, nframes = 200, fps = 7, start_pause = 5, end_pause = 5)
anim_save(file.path(hddir, 'timelapse', '2019-11-19', 'timelapse_anim_point.gif'),
          animation = p_anim)
