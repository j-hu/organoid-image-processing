#	Summarizes data in file.processed and creates a csv formatted as:
#	Expt Date	Strain	[Metadata]	% Correct	Total
metadata = c('date','timepoint','strain', indvars)

# load organization outcomes
data_original <- read.csv(file.processed, as.is=TRUE, stringsAsFactors=TRUE)
# don't edit the original dataframe
data <- data_original
# turn NA into empty string to allow comparisons
data[is.na(data)] <- ""
# throw away extra columns
data[,c('r','i','j','folder','imgtype','lumen','center')] <- NULL
# turn date strings into date (type). Sometimes Excel does weird things to it.
data$date <- as.Date(data$date, "%Y-%m-%d")
# convert indvar numeric codes to string factors
data <- name_indvars(data)
# convert timepoints to days
data$original.timepoint <- data$timepoint
result <- sapply(data$timepoint,tp_to_day)
data$timepoint <- result
data$timepoint <- factor(data$timepoint)
# set reduction for outcomes
data$original.outcome <- data$outcome
use_borderline_outcomes = FALSE
combine_successes = TRUE
source('reduce_plot_outcomes.r')
# unabbreviate everything in the outcome column to match strings in outcomes
result <- sapply(data$outcome, out_to_outcome)
# use reduce_outcome function from reduce_plot_outcomes.r
result <- sapply(result, reduce_outcome)
data$original.outcome <- data$outcome
data$outcome <- factor(result, levels = outcomes)
# merge degenerate viruses
data$original.virus <- data$virus
data$virus <- merge_viruses(data$virus)

rm('result')
write.csv(data, file = file.path(dir.results.org,'Summary','JH_data.csv'))

# Combine data from each experiment/conditions -----------------------------------------------------

data_summed <- aggregate(name ~ outcome+date+timepoint+strain+strainM+strainL+
    age+agegroup+site+confluence+ECM+drug+virus+FDG+CD10, data, length)
names(data_summed)[names(data_summed) == 'name'] <- 'count'
data_summed <- data_summed[,c(metadata,'count','outcome')]
# total organoids per metadata
data_fractions_jh <- aggregate(count ~ date+timepoint+strain+confluence+ECM+drug+virus+FDG+CD10,
							data_summed, sum)
names(data_fractions_jh)[names(data_fractions_jh) == 'count'] <- 'total'
data_fractions_jh <- merge(data_fractions_jh,
	data_summed[data_summed$outcome == 'correct',],
	by=metadata,all=TRUE)
names(data_fractions_jh)[names(data_fractions_jh) == 'count'] <- 'correct'
data_fractions_jh$outcome <- NULL
# this process makes NA if 0 correct, so replace NAs with 0
data_fractions_jh[is.na(data_fractions_jh$correct), 'correct'] <- 0

# fraction correct
data_fractions_jh$fraction <- data_fractions_jh$correct / data_fractions_jh$total
data_fractions_jh <- data_fractions_jh[,c('date','strain','timepoint','fraction','total',
	'virus','confluence','ECM','drug','FDG','CD10')]

# save results
write.csv(data_fractions_jh, file = file.path(dir.results.org,'Summary','JH_expts.csv'),
          quote = FALSE, na = "")

# Combine with Vasudha's summary data ------------------------------------------

VSfile <- file.path(dir.results.org,'Summary','VS_expts.csv')
data_fractions_vs <- read.csv(VSfile, stringsAsFactors = TRUE, na = '')
data_fractions_vs$X <- NULL
data_fractions_vs$drug <- ''
data_fractions_vs$virus <- merge_viruses(data_fractions_vs$virus)

data_fractions_jh$source <- 'JH'
data_fractions <- rbind(data_fractions_jh, data_fractions_vs)
data_fractions <- add_strain_info(data_fractions)
write.csv(data_fractions, file = file.path(dir.results.org,'Summary','unfiltered_expts.csv'),
          quote = FALSE, na = "")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

data_filtered <- filter_organization_data(data_fractions)
# factor date, virus, strain
factorize_metadata(data_filtered)
factorize_metadata(data_fractions)
# save results
write.csv(data_filtered,file=file.path(dir.results.org,'Summary','filtered_expts.csv'),
          quote = FALSE, na = "")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# do analysis on a subset of the data
data_subset <- data_filtered[
	data_filtered$confluence=='90%' & data_filtered$CD10 == 'all MEP' &
	data_filtered$FDG=='all',]

data_subset_summary <- combine_expts(data_subset)

# save results
write.csv(data_subset, file=file.path(dir.results.org,'Summary','filtered_subset.csv'),
          quote = FALSE, na = "")
write.csv(data_subset_summary, file=file.path(dir.results.org,'Summary','filtered_subset_combined.csv'),
          quote = FALSE, na = "")

# remove unfiltered dataframes
rm(list = c('data_fractions_jh','data_fractions_vs',
            'data_original','data_summed'))

for (column in c("date", "strain", "virus", "drug")) {
  nas <- data_filtered[is.na(data_filtered[, column]),]
  if (nrow(nas) > 1) {
    print(nas[,c("date", "strain", "virus", "drug")])
  }
}
