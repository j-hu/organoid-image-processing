source('constants.r')
source('constants_Boltzmann.r')
graphdir <- file.path(resultsdir, 'Graphs', 'Distributions')

## use real data ---------------------------------------------------------------
source('identify.r')
which(is.na(data$virus))
which(data$Edge_LEP_Fraction < 0)
data <- data[!is.na(data$virus) & data$uArea < 15000 & data$Edge_LEP_Fraction >= 0,]
data240L2 <- subset_by(data, s = c('240L', '240L M', '240L L'), d = c('DMSO',''))
data240L2 <- data240L2[data240L2$ECM == 'agarose' | (data240L2$timepoint == 2 & data240L2$ECM == 'Matrigel'),]
MF4060 <- data240L2$MEP_Fraction < .6 & data240L2$MEP_Fraction > .4

# from DAH Modeling:
# Wname      slope  intercept
# 1             GFP  880.82325 -2165.3714
# 2          PIK3CA  281.05016 -2251.9476
# 3      PIK3CA_cop  457.32874 -2097.3015
# 4       PIK3CA_MK  768.97484 -2539.3939
# 5       PIK3CA_EH   67.08565 -2155.7790
# 6        TLN1shMM  695.12328 -2587.5527
# 7      CTNND1shMM -570.89091 -2198.5195
# 8        PIK3CAMM -607.38864 -2282.9031
# 9        PIK3CALL -183.00267  -742.2011
# 10    GFP_agarose -107.47680  -879.0332
# 11 PIK3CA_agarose -107.47680  -879.0332

# Drug conditions
typenamesD <- c('GFP DMSO','GFP MK2206', 'GFP copanlisib',
                'H1047R DMSO','H1047R MK2206', 'H1047R copanlisib')
idx <- data$timepoint == 2 & data$MEP_Fraction < .6 & data$MEP_Fraction > .4 &
  data$ECM == 'Matrigel' & data$strain == '240L' & data$uArea > 3000 & data$uArea < 6000 &
  !(data$date %in% c('2020-06-16', '2020-06-23'))
typesetsD <- list(
  which(idx & data$virus == 'GFP' & data$drug == 'DMSO'),
  which(idx & data$virus == 'GFP' & data$drug == 'MK2206'),
  which(idx & data$virus == 'GFP' & data$drug == 'copanlisib'),
  which(idx & data$virus == 'H1047R' & data$drug == 'DMSO'),
  which(idx & data$virus == 'H1047R' & data$drug == 'MK2206'),
  which(idx & data$virus == 'H1047R' & data$drug == 'copanlisib'))
typeenergiesD <- list(880.82325, 880.82325, 880.82325, 281.05016, 281.05016, 281.05016)
names(typesetsD) <- typenamesD
names(typeenergiesD) <- typenamesD
# only keep those with a minimum length
typenamesD <- typenamesD[which(sapply(typesetsD, length) > 10)]
typesetsD <- typesetsD[typenamesD]
typeenergiesD <- typeenergiesD[typenamesD]
pdfD <- densify_sets(data, typesetsD)
pdfD[grepl('GFP', pdfD$Type), 'Virus'] <- 'GFP'
pdfD[grepl('H1047R', pdfD$Type), 'Virus'] <- 'H1047R'
pdfD[grepl('DMSO', pdfD$Type), 'Drug'] <- 'DMSO'
pdfD[grepl('MK2206', pdfD$Type), 'Drug'] <- 'MK2206'
pdfD[grepl('copanlisib', pdfD$Type), 'Drug'] <- 'copanlisib'
save_pngpdf(ggplot(pdfD[pdfD$Value %in% c('EdgeLEP'),],
                   aes(x = X, y = Density, color = Drug)) +
              facet_wrap(~ Virus) +
              labs(x = 'fraction boundary LEP', y = 'probability density') +
              geom_line(size = 1, alpha = 0.9),
            filename = file.path(graphdir, 'structure distributions - drug EL'), w = 7, h = 4)
save_pngpdf(ggplot(pdfD[pdfD$Value %in% c('Area'),],
                   aes(x = X, y = Density, color = Drug)) +
              facet_wrap(~ Virus) +
              labs(x = 'Area (um2)', y = 'probability density') +
              geom_line(size = 1, alpha = 0.9),
            filename = file.path(graphdir, 'structure distributions - drug size'), w = 7, h = 4)
save_pngpdf(ggplot(pdfD[pdfD$Value %in% c('MEP_Fraction'),],
                   aes(x = X, y = Density, color = Drug)) +
              facet_wrap(~ Virus) +
              labs(x = 'fraction MEP', y = 'probability density') +
              geom_line(size = 1, alpha = 0.9),
            filename = file.path(graphdir, 'structure distributions - drug MF'), w = 7, h = 4)


# look at frequency curves of GFP and PIK3CA -----------------------------------
typenamesGvH <- c('GFP', 'H1047R', 'GFP_agarose', 'PIK3CA_agarose',
                  'GFP 70%+MEP', 'GFP 60%+LEP', 'TLNshMM')
typesetsGvH <- list(
  which(data240L2$virus == 'GFP' & data240L2$ECM == 'Matrigel' & MF4060),
  which(data240L2$virus == 'H1047R' & data240L2$ECM == 'Matrigel' & MF4060),
  which(data240L2$virus == 'GFP' & data240L2$ECM == 'agarose' & MF4060),
  which(data240L2$virus%in% c('PIK3CA', 'H1047R') & data240L2$ECM == 'agarose' &
          MF4060),
  which(data240L2$virus == 'GFP' & data240L2$ECM == 'Matrigel' & data240L2$MEP_Fraction > 0.7),
  which(data240L2$virus == 'GFP' & data240L2$ECM == 'Matrigel' & data240L2$MEP_Fraction < 0.4),
  which(data240L2$virus %in% c('GFP_TLN1sh1puroM+mCh_puroM', 'TLN1shM+mChM') &
          MF4060 & data240L2$ECM == 'Matrigel'))
typeenergiesGvH <- list(880.82325, 281.05016, -107.47680, -107.47680, 880.82325, 880.82325, 695.1213)
names(typesetsGvH) <- typenamesGvH
names(typeenergiesGvH) <- typenamesGvH
# only keep those with a minimum length
typenamesGvH <- typenamesGvH[which(sapply(typesetsGvH, length) > 10)]
typesetsGvH <- typesetsGvH[typenamesGvH]
typeenergiesGvH <- typeenergiesGvH[typenamesGvH]
pdfGvH <- densify_sets(data240L2, typesetsGvH)
save_pngpdf(ggplot(pdfGvH[pdfGvH$Type %in% c('GFP', 'H1047R', 'GFP_agarose', 'PIK3CA_agarose'),],
                   aes(x = X, y = Density, color = Type)) + labs(x = '') +
              geom_line(size = 1, alpha = 0.9) + facet_wrap( ~ Value, scales = 'free') +
            scale_color_manual(values = c('green4','darkolivegreen2', 'blue4', 'skyblue2'), labels = c('GFP','GFP agarose','PIK3CA','PIK3CA agarose')),
            filename = file.path(graphdir, 'structure distributions'), w = 7, h = 4)
ggplot(pdfGvH[pdfGvH$Type %in% c('GFP', 'GFP_agarose', 'GFP 70%+MEP', 'GFP 60%+LEP'),],
       aes(x = X, y = Density, color = Type)) + labs(x = '') +
  geom_line(size = 1, alpha = 0.9) + facet_wrap( ~ Value, scales = 'free')

typenamesML <- c('Monly', 'Monlysmall', 'Monlybig', 'MonlyGreen')
typesetsML <- list(
                 which(data240L2$virus %in% c('GFPM+mChM') & MF4060),
                 which(data240L2$virus %in% c('GFPM+mChM') & MF4060 & data240L2$uArea < 5000),
                 which(data240L2$virus %in% c('GFPM+mChM') & MF4060 & data240L2$uArea > 10000),
                 which(data240L2$virus %in% c('GFPM+mChM') & data240L2$MEP_Fraction < 0.3)
                 )
names(typesetsML) <- typenamesML
pdfML <- densify_sets(data240L2, typesetsML)
save_pngpdf(ggplot(pdfML, aes(x = X, y = Density, color = Type)) + labs(x = 'Fraction') +
  geom_line(size = 1, alpha = 0.6) + facet_wrap( ~ Value, scales = 'free'),
  filename = file.path(graphdir, 'structure distributions homotypic'), w = 7, h = 4)
save_pngpdf(ggplot(pdfML[pdfML$Type %in% c('Monly','Monlysmall','Monlybig') & pdfML$Value != 'MEP_Fraction',],
                   aes(x = X, y = Density, color = Type)) + labs(x = 'Fraction') +
              geom_line(size = 1, alpha = 0.6) + facet_wrap( ~ Value, scales = 'free') +
              scale_color_discrete(labels = c('All', 'Small', 'Big'), name = 'MEP tissues'),
            filename = file.path(graphdir, 'structure distributions homotypic - sizes'), w = 6, h = 4)
save_pngpdf(ggplot(pdfML[pdfML$Type %in% c('Monly','MonlyGreen') & pdfML$Value != 'Area',],
                   aes(x = X, y = Density, color = Type)) + labs(x = 'Fraction') +
              geom_line(size = 1, alpha = 0.6) +
              facet_wrap( ~ Value, scales = 'free') +
              scale_color_discrete(labels = c('All', '70%+Green'), name = 'MEP tissues'),
            filename = file.path(graphdir, 'structure distributions homotypic - MF'), w = 6, h = 4)

## Exploring distributions that fit
ELMonly <- data240L2[typesetsML$Monly, 'Edge_LEP_Fraction']
descdist(ELMonly, boot = 5000)
descdist(data240L2[typesetsML$MonlyGreen, 'Edge_LEP_Fraction'], boot = 5000)
ggplot(pdfML[pdfML$Type %in% c('Monly','MonlyGreen'),], aes(x = X, y = Density, color = Type)) + labs(x = '') +
  geom_line(size = 1, alpha = 0.6) + facet_wrap( ~ Value, scales = 'free')
fn <- fitdist(ELMonly, 'norm')
fln <- fitdist(ELMonly, 'lnorm')
fw <- fitdist(ELMonly, 'weibull')
fg <- fitdist(ELMonly, 'gamma')

descdist(data240L2[typesetsGvH$GFP, 'Edge_LEP_Fraction'], boot = 5000)
descdist(data240L2[typesetsGvH$H1047R, 'Edge_LEP_Fraction'], boot = 5000)
ggplot(pdfGvH[pdfGvH$Type %in% c('GFP','H1047R'),], aes(x = X, y = Density, color = Type)) + labs(x = '') +
  geom_line(size = 1, alpha = 0.6) + facet_wrap( ~ Value, scales = 'free')
fn <- fitdist(data240L2[typesetsGvH$GFP, 'Edge_LEP_Fraction'], 'norm')
fln <- fitdist(data240L2[typesetsGvH$GFP, 'Edge_LEP_Fraction'], 'lnorm')
fw <- fitdist(data240L2[typesetsGvH$GFP, 'Edge_LEP_Fraction'], 'weibull')
fg <- fitdist(data240L2[typesetsGvH$GFP, 'Edge_LEP_Fraction'], 'gamma')
denscomp(list(fw, fln, fg, fn))
summary(fn); summary(fw)
gofstat(list(fw, fln, fg, fn))


## QQ plot of MLonly ---------------------------------------------------------
qqnorm(ELMonly); qqline(ELMonly)
# KS test
ks.test(ELMonly, 'pnorm', mean(ELMonly), sd(ELMonly))
# features of MLonly compared to others
ggplot(data = data240L2[typesetsML$Monly,], aes(x = uArea)) +
  geom_histogram(binwidth = 500) + labs(title = "MEPonly 40-60%Red") +
  lims(x = c(0, 15000))
ggplot(data = data240L2[typesetsGvH$GFP,], aes(x = uArea)) +
  geom_histogram(binwidth = 500) + labs(title = 'GFP 40-60%MEP')  +
  lims(x = c(0, 15000))
ggplot(data = data240L2[typesetsGvH$H1047R,], aes(x = uArea)) +
  geom_histogram(binwidth = 500) + labs(title = 'H1047R 40-60%MEP')  +
  lims(x = c(0, 15000))
ggplot(data = data240L2[typesetsGvH$GFP_agarose,], aes(x = uArea)) +
  geom_histogram(binwidth = 500) + labs(title = 'GFP agarose 40-60%MEP')  +
  lims(x = c(0, 15000))

ggplot(data = data240L2[typesetsML$Monly,], aes(x = MEP_Fraction)) +
  geom_histogram(binwidth = 0.02) + labs(title = "MEPonly 40-60%Red") +
  lims(x = c(0, 1))
ggplot(data = data240L2[typesetsGvH$GFP,], aes(x = MEP_Fraction)) +
  geom_histogram(binwidth = 0.02) + labs(title = 'GFP 40-60%MEP')  +
  lims(x = c(0, 1))

# invert the pixels
iMdata <- data240L2[typesetsML$Monly, ]
iMdata$MEP_Fraction <- 1-iMdata$MEP_Fraction
iMdata$Edge_LEP_Fraction <- 1-iMdata$Edge_LEP_Fraction
ggplot(iMdata, aes(x = MEP_Fraction)) +
  geom_histogram(binwidth = 0.02) + labs(title = "Inverted MEPonly 40-60%Red") +
  lims(x = c(0, 1))

# use inverted pixels data to generate the normal distribution of EdgeLEP
m = mean(iMdata$Edge_LEP_Fraction)
s = sd(iMdata$Edge_LEP_Fraction)
normal = data.frame(Edge_LEP_Fraction = (0:100)/100, Density = dnorm((0:100)/100, mean = m, sd = s))
ggplot(iMdata, aes(x = Edge_LEP_Fraction)) +
  geom_histogram(binwidth = 0.02, alpha = 0.8) +
  geom_line(data=normal, aes(y = Density), size = 2) +
  labs(title = "Inverted MEPonly 40-60%Red") +
  lims(x = c(0, 1))
ggplot(iMdata, aes(x = Edge_LEP_Fraction)) +
  geom_density(alpha = 0.8) +
  geom_line(data=normal, aes(y = Density), linetype = 'dashed') +
  labs(title = "Inverted MEPonly 40-60%Red") +
  lims(x = c(0, 1))
normMonly <- bind_rows(lapply(typenamesML, FUN = function(type) {
  F <- function(x, m, sd) { dnorm(x, m, sd) }
  mdl <- nls(Density ~ F(X, m, sd),
             data = pdfML[pdfML$Type == type & pdfML$Value == 'EdgeLEP',],
             start = list(m = 0.5, sd = 0.15), control = list(warnOnly = TRUE))
  mdldata <- rbind(data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = fitted(mdl), Value = 'Fitted'),
                   data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = residuals(mdl), Value = 'Residual'),
                   data.frame(EdgeLEP = pdfML[pdfML$Type == type & pdfML$Value == 'EdgeLEP','X'],
                              Density = pdfML[pdfML$Type == type & pdfML$Value == 'EdgeLEP','Density'],
                              Type = type, Value = 'Original'))
  print(sprintf('%s: m = %g, sd = %g', type, coefficients(mdl)[['m']], coefficients(mdl)[['sd']]))
  return(mdldata)
}))
save_pngpdf(ggplot(normMonly[normMonly$Value %in% c('Fitted', 'Original'),],
                aes(x = EdgeLEP, y = Density, linetype = Value, color = Type)) +
              geom_line() + facet_wrap(~ Type, labeller = condition.labels) +
              scale_linetype_manual(values = c('dashed','solid')) +
              scale_color_discrete(labels = c('All', 'Big', 'Small', '70%+ Green')),
         filename = file.path(graphdir, 'MEPonly fits Gaussian'), w = 4, h = 5)

# use this to make Boltzmann distribution for G, H -----------------------------
mdldataGvH <- bind_rows(lapply(typenamesGvH, FUN = function(type) {
  F <- BoltzmannF(MFs = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'MEP_Fraction',],
                  type = 'alphag', Ed = typeenergiesGvH[[type]], m = m, sd = s)
  mdl <- nls(Density ~ F(X, alpha),
             data = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'EdgeLEP',],
             start = list(alpha = 100), control = list(warnOnly = TRUE))
  mdldata <- rbind(data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = fitted(mdl), Value = 'Fitted'),
                   data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = residuals(mdl), Value = 'Residual'),
                   data.frame(EdgeLEP = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'EdgeLEP','X'],
                              Density = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'EdgeLEP','Density'],
                              Type = type, Value = 'Original'))
  print(sprintf('%s: alpha = %g', type, coefficients(mdl)[['alpha']]))
  return(mdldata)
}))
save_pngpdf(ggplot(mdldataGvH[mdldataGvH$Value %in% c('Fitted', 'Original') &
                                mdldataGvH$Type != 'TLNshMM',],
       aes(x = EdgeLEP, y = Density, color = Type, linetype = Value)) +
  geom_line() + facet_wrap(~ Type, labeller = condition.labels) +
    scale_linetype_manual(values = c('dashed','solid')) +
    scale_color_manual(values = c('green4','darkolivegreen2', 'blue4', 'skyblue2'),
                       labels = c('GFP','GFP agarose','PIK3CA','PIK3CA agarose')) +
    labs(x = 'Fraction Edge LEP'),
  filename = file.path(graphdir, 'Boltzmann fits'), w = 4, h = 5)

write.csv(pdfGvH, file.path(resultsdir, 'Densities', 'GvH.csv'))
write.csv(pdfML, file.path(resultsdir, 'Densities', 'ML.csv'))

mdldataGvH <- bind_rows(lapply(typenamesGvH[typenamesGvH != 'GFP 70%+MEP'], FUN = function(type) {
  F <- BoltzmannF(MFs = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'MEP_Fraction',],
                  type = 'alphaN', Ed = typeenergiesGvH[[type]])
  mdl <- nls(Density ~ F(X, alpha, Ntot),
             data = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'EdgeLEP',],
             start = list(alpha = 100, Ntot = 20), algorithm = 'port',
             lower = c(0, 4),
             upper = c(Inf, 29701),
             control = list(warnOnly = TRUE))
  mdldata <- rbind(data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = fitted(mdl), Value = 'Fitted'),
                   data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = residuals(mdl), Value = 'Residual'),
                   data.frame(EdgeLEP = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'EdgeLEP','X'],
                              Density = pdfGvH[pdfGvH$Type == type & pdfGvH$Value == 'EdgeLEP','Density'],
                              Type = type, Value = 'Original'))
  print(sprintf('%s: alpha = %g, Ntot = %g', type, coefficients(mdl)[['alpha']], coefficients(mdl)[['Ntot']]))
  return(mdldata)
}))
mdldataML <- bind_rows(lapply(typenamesML, FUN = function(type) {
  F <- BoltzmannF(MFs = pdfML[pdfML$Type == type & pdfML$Value == 'MEP_Fraction',],
                  type = 'Ntot', Ed = typeenergiesGvH[[type]])
  mdl <- nls(Density ~ F(X, Ntot),
             data = pdfML[pdfML$Type == type & pdfML$Value == 'EdgeLEP',],
             start = list(Ntot = 20), algorithm = 'port',
             lower = c(min(Ntots)),
             upper = c(max(Ntots)),
             control = list(warnOnly = TRUE))
  mdldata <- rbind(data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = fitted(mdl), Value = 'Fitted'),
                   data.frame(EdgeLEP = (0:511)/511, Type = type,
                              Density = residuals(mdl), Value = 'Residual'),
                   data.frame(EdgeLEP = pdfML[pdfML$Type == type & pdfML$Value == 'EdgeLEP','X'],
                              Density = pdfML[pdfML$Type == type & pdfML$Value == 'EdgeLEP','Density'],
                              Type = type, Value = 'Original'))
  print(sprintf('%s: Ntot = %g', type, coefficients(mdl)[['Ntot']]))
  return(mdldata)
}))
ggplot(mdldataGvH, aes(x = EdgeLEP, y = Density, color = Value), group = Value) +
  geom_line() + facet_wrap(~ Type)
save_pngpdf(ggplot(mdldataML[mdldataML$Value != 'Residual' & mdldataML$Type != 'MonlyGreen',],
       aes(x = EdgeLEP, y = Density, color = Type, linetype = Value)) +
    geom_line() + facet_wrap(~ Type, labeller = condition.labels) +
      scale_linetype_manual(values = c('dashed','solid')) +
      labs(x = 'Fraction Edge LEP'),
  filename = file.path(graphdir, 'MEPonly fits halfNtot'), w = 7, h = 3)

# after running fitBoltzmann in MATLAB -----------------------------------------
nlmGvH <- read.csv(file.path(resultsdir, 'Densities', 'GvH fit.csv'))
nlmML <- read.csv(file.path(resultsdir, 'Densities', 'ML fit.csv'))

# compare results
ggplot(nlmGvH, aes(x = EdgeLEP, y = Density)) +
  geom_line(size = 1, alpha = 0.8, aes(color = N)) + lims(x = c(0,1)) + # guides(color = 'none') +
  geom_line(data = pdfGvH[pdfGvH$Value == 'EdgeLEP' & pdfGvH$Type %in% c('GFP', 'H1047R'),],
            aes(x = X), size = 1.5, alpha = 0.8) + facet_wrap( ~ Type) +
  labs(x = 'Fraction Edge LEP', title = 'Fitted Boltzmann distributions')
p <- ggplot(nlmGvH[nlmGvH$Type %in% c('GFP', 'H1047R') & nlmGvH$N == 12,],
            aes(x = EdgeLEP, y = Density, color = Type)) +
  geom_line(size = 1.5, alpha = 0.8, linetype = 'dashed') +
  lims(x = c(0,1)) + scale_color_manual(values = c_GFPH1047R) +
  geom_line(data = pdf[pdf$Value == 'EdgeLEP' & pdf$Type %in% c('GFP', 'H1047R'),],
            aes(x = X), size = 1.5) +
  labs(x = 'Fraction Edge LEP', title = 'Fitted Boltzmann distributions, N = 12')
print(p)
save_pdf(p, filename = file.path(graphdir, 'GvH+-DMSO EdgeLEP fit Ntot12'), w = 5, h = 5)

p <- ggplot(nlmML, aes(x = EdgeLEP, y = Density, color = Type)) +
  geom_line(size = 1.5, alpha = 0.8, linetype = 'dashed') + lims(x = c(0,1)) +
  geom_line(data = pdfML[pdfML$Value == 'EdgeLEP',],
            aes(x = X), size = 1.5) + facet_wrap(~ Type) +
  labs(x = 'Fraction Edge LEP', title = 'MEP/LEP only, Ntot = 10.5')
print(p)
save_pdf(p, filename = file.path(graphdir, 'ML EdgeLEP fit'), w = 5, h = 5)

# save the RDS data together
saveRDS(pdfGvH[pdfGvH$Value == 'EdgeLEP' & pdfGvH$Type %in% c('GFP', 'H1047R'),],
        file = file.path(resultsdir, 'Densities', 'GvH+-DMSO EdgeLEP.rds'))
saveRDS(nlmGvH[nlmGvH$Type %in% c('GFP', 'H1047R') & nlmGvH$N == 12,],
        file = file.path(resultsdir, 'Densities', 'GvH+-DMSO EdgeLEP fit Ntot12.rds'))
saveRDS(pdfML, file.path(resultsdir, 'Densities', 'MEPLEPonly.rds'))
saveRDS(nlmML, file.path(resultsdir, 'Densities', 'MEPLEPonly fit.rds'))

## Random sampling power analysis ----------------------------------------------
modeldata <- nlm[nlm$Type == 'mChM+GFPMbig',]
samplesizes <- c(10, 20, 30, 40, 50, 70, 100, 150, 200, 300, 500, 1000)
samples <- bind_rows(lapply(samplesizes, FUN = function(n) {
  data.frame(EdgeLEP = sample(modeldata$EdgeLEP, size = n, prob = modeldata$Density, replace = TRUE), Type = n)
}))
typenames <- samplesizes
typesets <- lapply(samplesizes, FUN = function(n) { which(samples$Type == n) })
pdf <- densify(samples, 'EdgeLEP', typenames = typenames, whiches = typesets)
ggplot(pdf, aes(x = EdgeLEP, y = Density)) + geom_line(size = 1, alpha = 0.5) +
  geom_histogram(data = samples, aes(y = ..density..),
                 alpha = 0.5, binwidth = 0.05) + facet_wrap( ~ Type)
write.csv(pdf[,c('Type','EdgeLEP','Density')], file.path(resultsdir, 'Densities', 'samplesizes EdgeLEP.csv'))
nlm <- read.csv(file.path(resultsdir, 'Densities', 'samplesizes fitnlm.csv'))
# compare results
ggplot(nlm, aes(x = EdgeLEP, y = Density)) +
  geom_line(size = 1, alpha = 0.8, aes(color = Type)) + lims(x = c(0,1)) + # guides(color = 'none') +
  geom_line(data = pdf, size = 1.5, alpha = 0.8) + facet_wrap( ~ Type) +
  labs(x = 'Fraction Edge LEP', title = 'Fitted Boltzmann distributions')
ggplot(nlm, aes(x = Type, y = R2)) + geom_point(size = 4) + labs(x = 'Sample size')

## Boltzmann sensitivity analysis -----------------------------------------------
data_sen <- read.csv(file.path(resultsdir, 'Boltzmann sensitivity.csv'))
data_sen[data_sen$Ed == 750.2, 'Type'] <- 'GFP'
data_sen[data_sen$Ed == 239.3, 'Type'] <- 'PIK3CA'
data_sen[is.na(data_sen$Density), 'Density'] <- 0

p <- ggplot(data_sen[data_sen$N > 6 & !is.na(data_sen$Type) & data_sen$A > 10,],
            aes(x = EdgeLEP, y = Density, color = A, group = A)) +
  geom_line(size = 1, alpha = 0.8) + lims(x = c(0,1)) + #guides(color = 'none') +
  facet_grid(. ~ Type) +
  labs(x = 'Fraction Edge LEP', title = 'Boltzmann distributions')
p
save_pdf(p, filename = file.path(resultsdir, 'Graphs', 'Boltzmann sensitivity'), w = 5, h = 3)

# convert to fraction correct (EdgeLEP <= 0.3)
datasenfc <- unique(data_sen[data_sen$A != 0, c('Type', 'Ed', 'A', 'N')])
for (i in 1:nrow(datasenfc)) {
  subdata <- data_sen[data_sen$Ed == datasenfc[i, 'Ed'] &
                        data_sen$A == datasenfc[i, 'A'] &
                        data_sen$N == datasenfc[i, 'N'],]
  if (all(subdata$EdgeLEP > 0.3)) { datasenfc[i, 'FC'] <- 0
  } else if (all(subdata$EdgeLEP <= 0.3)) { datasenfc[i, 'FC'] <- 1 } else {
    datasenfc[i, 'FC'] <- sum(subdata[subdata$EdgeLEP <= 0.3, 'Density'])
    datasenfc[i, 'FI'] <- sum(subdata[subdata$EdgeLEP > 0.3, 'Density'])
    datasenfc[i, 'FC'] <- datasenfc[i, 'FC']/(datasenfc[i, 'FC'] + datasenfc[i, 'FI'])
  }
}
datasenfc$FI <- 1 - datasenfc$FC

ggplot(datasenfc[datasenfc$N == 12 & !is.na(datasenfc$Type),],
       aes(x = A, y = FC, color = Type, group = Type)) +
  geom_line(size = 2, alpha = 0.8) + guides(color = 'none') +
  labs(x = 'activity', title = 'Fraction correct') + lims(x = c(0, 100))


p <- ggplot(data = datasenfc[datasenfc$N == 12 & datasenfc$A <= 100,], aes(x = Ed, y = A)) +
  geom_raster(aes(fill = FC)) +
  scale_fill_gradient(name = 'Fraction correct', limits = c(0,1),
                      high = 'royalblue', low = 'khaki1') +
  theme_classic() + lims(x = c(800, 0)) +
  labs(x = 'E_d', y = 'Activity', title = 'Fraction of tissues with > 30% MEP in boundary')
p
filename <- file.path(resultsdir, 'Graphs', 'Boltzmann FC heatmap')
save_pdf(p, filename = filename, h = 5, w = 7)
save_png(p, filename = filename, h = 500, w = 700)

datasenfc$EA <- datasenfc$E/datasenfc$A
ggplot(data = datasenfc, aes(x = N, y = EA)) +
  geom_tile(aes(fill = FC)) +
  scale_fill_gradient(name = 'Fraction correct', limits = c(0,1),
                      high = 'royalblue', low = 'khaki1') +
  theme_classic() +
  labs(x = 'N', y = 'E/A', title = 'Fraction of tissues with > 30% MEP in boundary')


## Testing different parameters -------------------------------------------------
F <- BoltzmannF(type = 'alphaN', Ed = 750.2)
activities = c(1, 5, 10, 50, 100)
Ntots = c(10, 20, 30, 50, 100, 200)
datasen <- bind_rows(lapply(activities, FUN = function(alpha) {
  bind_rows(lapply(Ntots, FUN = function(Ntot) {
    x <- (1:100)/100
    data.frame(EdgeLEP = x, Density = F(x, alpha, Ntot),
               A = alpha, N = Ntot)
  }))
}))
p <- ggplot(datasen, aes(x = EdgeLEP, y = Density)) +
  geom_line(size = 1.5, alpha = 0.8) + lims(x = c(0,1)) +
  labs(x = 'Fraction Edge LEP') + facet_grid(A ~ N, scales = 'free')
print(p)

