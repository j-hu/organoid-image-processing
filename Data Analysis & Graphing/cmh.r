# Performs Cochran-Mantel-Haensel (CMH) chi-squared tests of significance.
# input: dataframe of the form binom_data_cropped with two ids
# output: the CMH test statistic
# appears to work as of 4/3. Keep dates as factors, that seemed to help

cmh <- function(data) {
	# only two ids, please
	ids <- unique(data$id)
	if (length(ids) > 2) {
		print("Only two ids at a time, please.")
		return
	}
	A <- ids[1]
	B <- ids[2]
	expts <- unique(data$date)

	# from contingency table for each experiment, calculate n11k-E(n11k) and var(niik)
	# s stands for "sum over this index" in table:
	#      success   failure   total
	#  A    n11k 	  n12k     n1sk
	#  B    n21k 	  n22k     n2sk
	#       ns1k      ns2k     nssk
	contingify <- function(expt_date) {
		subdata <- data[data$date == expt_date,]
		# both A and B need to be present
		if (!(A %in% unique(subdata$id)) || !(B %in% unique(subdata$id))) {
			return(c(NA,NA))
		}
		n11k <- subdata[subdata$id == A, "success"]
		n1sk <- subdata[subdata$id == A, "total"]
		ns1k <- subdata[subdata$id == A, "success"] + subdata[subdata$id == B, "success"]
		nssk <- subdata[subdata$id == A, "total"] + subdata[subdata$id == B, "total"]
		n2sk <- subdata[subdata$id == B, "total"]
		ns2k <- subdata[subdata$id == A, "failure"] + subdata[subdata$id == B, "failure"]
		E_n11k <- n1sk*ns1k/nssk
		err <- n11k-E_n11k
		v_n11k <- n1sk*n2sk*ns1k*ns2k /(nssk^2*(nssk-1))
		answer <- c(err, v_n11k)
		return(answer)
	}
	stats <- data.frame(lapply(expts, contingify))
	rownames(stats) <- c("err", "var")
	colnames(stats) <- expts
	# drop NA columns
	for (expt_date in expts)
		if (!is.null(stats["err", expt_date]) && is.na(stats["err", expt_date]))
			stats[,expt_date] <- NULL
	# CMH = squared sum of errors / sum of variance
	test_statistic <- sum(stats["err",])^2 / sum(stats["var",])
	return(test_statistic)
}