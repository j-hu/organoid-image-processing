# edit this code to change what experiment is plotted

jhudir <- '/Users/jhu/Box/Gartnerlab Data/Individual Folders/Jennifer Hu'
codedir <- file.path(jhudir, 'Analysis', 'Organoid Image Processing', 'Data Analysis & Graphing')

for (expt in expts) {
  rmarkdown::render(input = file.path(codedir,
                                      'organization_assay_report.Rmd'),
                    params = list(expt=expt, codedir=codedir),
                    output_format = "html_document",
                    output_file = paste0(expt,".html"),
                    output_dir = file.path(jhudir, 'Data', 'Organization Data',
                                           'Results', 'Summary', 'html'),
                    envir = new.env())
}
