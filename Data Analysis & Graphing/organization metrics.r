source('identify.r')

hist_fraction <- ggplot(data, aes(x = MEP_Fraction)) +
  geom_histogram(bins = 30) +
  labs(title = 'MEP Fraction Distribution', x = 'MEP Fraction')
print(hist_fraction)
hist_circ <- ggplot(data, aes(x = Organoid_Circularity)) +
  geom_histogram(bins = 30) +
  labs(title = 'Organoid Circularity Distribution', x = 'Circularity')
print(hist_circ)
hist_area <- ggplot(data, aes(x = uArea)) +
  geom_histogram(bins = 30) + # scale_x_continuous(trans='log2') +
  labs(title = 'Organoid Area Distribution', x = 'Area (um^2)')
print(hist_area)
density_ratio_out <- ggplot(data, aes(x = MEP_Fraction, color = outcome)) +
  geom_density(size = 2, alpha = 0.8) +
  labs(x = 'MEP Fraction')
print(outcomes_fill(density_ratio_out))
density_circ_out <- ggplot(data, aes(x = Organoid_Circularity, color = outcome)) +
  geom_density(size = 2, alpha = 0.8) +
  labs(x = 'Circularity')
print(outcomes_fill(density_circ_out))
density_area_out <- ggplot(data, aes(x = uArea, color = outcome)) +
  geom_density(size = 2, alpha = 0.8) + scale_x_continuous(trans = 'log2') +
  labs(x = 'Area (um^2)')
print(outcomes_fill(density_area_out))

# measures of compactness
p <- ggplot(datalong, aes(x = outcome, y = Solidity, fill = Metric_Lineage)) +
  geom_boxplot() + scale_y_continuous(trans = 'log2') +
  scale_fill_manual(values = c_LEPMEP, name = 'Lineage') +
  labs(x = 'Outcome', y = 'Solidity', title = 'LEP and MEP Solidity by Outcome')
save_png(p, filename = file.path(graphdir, 'metrics_solidity'), w = 400, h = 350)
p <- ggplot(datalong, aes(x = outcome, y = Intercentroid, fill = outcome)) +
  geom_boxplot(show.legend = FALSE) +
  labs(y = 'Intercentroid Distance', title = 'Distance between LEP and MEP Centroids')
save_png(outcomes_fill(p), filename = file.path(graphdir, 'metrics_intercentroid'), w = 400, h = 350)

# polarity
p <- ggplot(data, aes(x = Outer_MEP_Score, y = Inner_LEP_Score, color = outcome)) +
  geom_point(size = 3, alpha = 0.3) +
  labs(x = 'Outer MEP Score', y = 'Inner LEP Score', title = 'LEP/MEP Enrichment in Organoid Regions')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment'), w = 400, h = 500)
p <- ggplot(data, aes(x = Outer_MEP_Fraction, y = Inner_LEP_Fraction, color = outcome)) +
  geom_point(size = 3, alpha = 0.3) +
  labs(x = 'Outer MEP Fraction', y = 'Inner LEP Fraction', title = 'LEP/MEP Fractions in Organoid Regions')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment_fraction'), w = 400, h = 500)
icmax = round(max(data$Intercentroid)+0.1, digits = 2)
p <- ggplot(data, aes(x = Outer_MEP_Score, y = Intercentroid, color = outcome)) +
  geom_point(size = 3, alpha = 0.3) + lims(x = c(0,1), y = c(0, icmax)) +
  labs(x = 'Outer MEP Score', y = 'Intercentroid Distance')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment_intercentroid'), w = 400, h = 500)
p <- ggplot(data, aes(x = Outer_MEP_Score, y = Intercentroid, color = outcome)) +
  geom_point(size = 2, alpha = 0.1) + lims(x = c(0,1), y = c(0, icmax)) +
  labs(x = 'Outer MEP Score', y = 'Intercentroid Distance')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment_intercentroid'), w = 400, h = 500)
p <- ggplot(data, aes(x = Outer_MEP_Score, y = Intercentroid, color = outcome)) +
  geom_density2d(size = 2, alpha = 0.8) + lims(x = c(0,1), y = c(0, icmax)) +
  labs(x = 'Outer MEP Score', y = 'Intercentroid Distance')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment_intercentroid_density2d'), w = 400, h = 500)
p <- ggplot(data, aes(x = Outer_MEP_Score*Inner_LEP_Score, y = Intercentroid, color = outcome)) +
  geom_point(size = 2, alpha = 0.1) + lims(x = c(0,1), y = c(0, icmax)) +
  labs(x = 'Outer MEP & Inner LEP Score', y = 'Intercentroid Distance')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment2_intercentroid'), w = 500, h = 500)
p <- ggplot(data, aes(x = Outer_MEP_Score*Inner_LEP_Score, y = Intercentroid, color = outcome)) +
  geom_density2d(size = 2, alpha = 0.8) + lims(x = c(0,1), y = c(0, icmax)) +
  labs(x = 'Outer MEP Score & Inner LEP Score', y = 'Intercentroid Distance')
p <- outcomes_color(p)
save_png(p, filename = file.path(graphdir, 'metrics_enrichment2_intercentroid_density2d'), w = 500, h = 500)

# Compare GFP and PIK3CA -------------------------------------------------------
datasubset <- subset_by(data, s = '240L', v = c('GFP', 'H1047R'), d = '', e = 'Matrigel')
# random subsample GFP down to match
n_subset <- sum(datasubset$virus == 'H1047R')
idxGFP <- which(datasubset$virus == 'GFP')
idxGFP_n <- sample(idxGFP, size = n_subset, replace = FALSE)
datasubset <- datasubset[c(idxGFP_n, which(datasubset$virus == 'H1047R')),]

p <- ggplot(datasubset, aes(x = Outer_MEP_Score, y = Intercentroid, color = virus)) +
  geom_point(size = 2, alpha = 0.8, show.legend = FALSE) +
  scale_color_manual(values = c_GFPH1047R, name = 'Transgene') +
  lims(x = c(0,1), y = c(0, icmax)) +
  labs(x = 'Outer MEP Score', y = 'Intercentroid Distance') + facet_grid(. ~ virus)
print(p)
save_png(p, filename = file.path(graphdir, 'GvH_enrichment_intercentroid_scatter'), w = 800, h = 500)
p <- ggplot(datasubset, aes(x = Outer_MEP_Score, y = Intercentroid, color = virus)) +
  geom_density2d(size = 2, alpha = 0.8, show.legend = FALSE) +
  scale_color_manual(values = c_GFPH1047R, name = 'Transgene') +
  labs(x = 'Outer MEP Score', y = 'Intercentroid Distance') + facet_grid(. ~ virus)
save_png(p, filename = file.path(graphdir, 'GvH_enrichment_intercentroid_density2d'), w = 800, h = 500)

ggplot(datasubset, aes(x = MEP_Fraction, color = virus)) + geom_density()
ggplot(datasubset, aes(x = MEP_Fraction, y = Outer_LEP_Score)) + geom_density2d() +
  facet_grid(. ~ virus)


hist_ratio <- ggplot(datasubset, aes(x = MEP_Fraction)) +
  geom_histogram(bins = 30) +
  labs(title = 'MEP Fraction Distribution', x = 'MEP Fraction')
hist_circ <- ggplot(datasubset, aes(x = Organoid_Circularity)) +
  geom_histogram(bins = 30) +
  labs(title = 'Organoid Circularity Distribution', x = 'Circularity')
hist_area <- ggplot(datasubset, aes(x = uArea)) +
  geom_histogram(bins = 30) +  scale_x_continuous(trans='log2') +
  labs(title = 'Organoid Area Distribution', x = 'Area (um^2)')
density_ratio_out <- ggplot(datasubset, aes(x = MEP_Fraction, fill = virus)) +
  geom_density(show.legend = FALSE, alpha = 0.8) +
  labs(x = 'MEP Fraction')+ scale_fill_manual(values = c_GFPH1047R)
density_circ_out <- ggplot(datasubset, aes(x = Organoid_Circularity, fill = virus)) +
  geom_density(show.legend = FALSE, alpha = 0.8) +
  labs(x = 'Circularity')+ scale_fill_manual(values = c_GFPH1047R, name = 'Transgene')
# distribution of area, colored by outcome
density_area_out <- ggplot(datasubset, aes(x = uArea, fill = virus)) +
  geom_density(show.legend = FALSE, alpha = 0.8) +
  scale_x_continuous(trans='log2') + scale_fill_manual(values = c_GFPH1047R) +
  labs(x = 'Area (um^2)')

## ----------------
datasubset <- subset_by(data, s = '240L', e = 'Matrigel', v = c('GFP','mChL+GFPL'), d = '')
ggplot(datasubset, aes(x = MEP_Fraction, y = Outer_MEP_Score)) +
  geom_point() + facet_grid(. ~ virus)


## --------------
datasubset <- subset_by(data, s = '240L',
                        v = c('GFP', 'H1047R', 'MYC', 'RB1sh', 'D1', 'ERBB2', 'E17K', 'p16sh', 'KRAS'),
                        d = '', e = 'Matrigel')
p <- ggplot(datasubset, aes(x = Outer_MEP_Score, y = Intercentroid, color = virus)) +
  geom_density2d(size = 1.5, alpha = 0.8, show.legend = FALSE) +
  labs(x = 'Outer MEP Enrichment', y = 'Intercentroid Distance') + facet_wrap(~ virus)
save_png(p, filename = file.path(graphdir, 'viruses_enrichment_intercentroid_density2d'), w = 1600, h = 1400)

p <- ggplot(datasubset, aes(x = 1-Outer_MEP_Score, fill = virus)) +
  geom_density(alpha = 0.8) +
  labs(x = 'Outer LEP Score', y = 'Density', title = 'Outer Region LEP Score')
p