quantidata <- read.csv(quantifile,as.is=TRUE,stringsAsFactors=TRUE,row.names=NULL)
duplicates <- table(quantidata$imfilename)[table(quantidata$imfilename) > 1]
# this is because there are duplicate images from different Ilastik runs
if (length(duplicates) > 0) {
  idx <- sapply(names(duplicates), FUN = function(x) {
    min(which(quantidata$imfilename == x))
  })
}
quantidata <- quantidata[-idx,]
write.csv(quantidata, quantifile, quote = FALSE, row.names = FALSE)
