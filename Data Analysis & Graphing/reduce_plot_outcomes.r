
print(sprintf("Combine successes = %d. Borderlines = %d. Generating colors accordingly.", 
              combine_successes, use_borderline_outcomes))

# Determines colors, order, and number of outcomes plotted.
if (combine_successes) {
  if (use_borderline_outcomes) {
    outcomes = c("incorrect","borderline","correct")
    outcome_labels = c("incorrect","borderline","correct")
    outcome_colors = c("dimgray","darkseagreen4","darkseagreen3")
  } else {
    outcomes = c("incorrect","correct")
    outcome_labels = c("incorrect","correct")
    outcome_colors = c("dimgray","darkseagreen3")
  }
} else if (use_borderline_outcomes) {
  outcomes = all_outcomes
  outcome_labels = c("correct/split", "correct", "correct/mixed", "mixed", 
                     "inverted/mixed", "inverted", "inverted/split", "split")
  outcome_colors = c("greenyellow", "limegreen", "darkgreen", "firebrick", 
                     "darkorchid4", "royalblue", "cyan2", "gold1")	
} else {
  outcomes = c("correct","mixed","inverted","split")
  outcome_labels = c("correct","mixed","inverted","split")
  outcome_colors = c("limegreen","firebrick", "royalblue","gold1")
}

# Determines which outcomes get collapsed into others.
# "correct.split", "correct", "correct.mixed", "mixed",
#	"inverted.mixed", "inverted", "inverted.split", "split"
reduce_outcome = function(outcome) {
   if (combine_successes) {
     if (use_borderline_outcomes) {
       reduced = switch(outcome,
                        correct.split = "borderline",
                        correct = "correct",
                        correct.mixed = "borderline",
                        mixed = "incorrect",
                        inverted.mixed = "incorrect",
                        inverted = "incorrect",
                        inverted.split = "incorrect",
                        split = "incorrect")
     } else {
       reduced = switch(outcome,
                        correct.split = "incorrect",
                        correct = "correct",
                        correct.mixed = "incorrect",
                        mixed = "incorrect",
                        inverted.mixed = "incorrect",
                        inverted = "incorrect",
                        inverted.split = "incorrect",
                        split = "incorrect")
     }
   } else {
     reduced = switch(outcome,
                      correct.split = "split",
                      correct = "correct",
                      correct.mixed = "mixed",
                      mixed = "mixed",
                      inverted.mixed = "mixed",
                      inverted = "inverted",
                      inverted.split = "split",
                      split = "split")
   }
   return(reduced)
}

outcomes_color = function(g) {
  g <- g + scale_color_manual(values = outcome_colors,labels = outcome_labels)
  return(g)
}
outcomes_fill = function(g) {
  g <- g + scale_fill_manual(values = outcome_colors,labels = outcome_labels)
  return(g)
}