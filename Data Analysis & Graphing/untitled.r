# Assorted small tasks/plots.
source('constants.r')
source('classed_summarize.r')
source('identify.r')

# Find mixed organoid images --------------------------------------------------
data$Components <- data$LEP_Components + data$MEP_Components
subdata <- data[data$Pixels < 50000 & data$MEP_Fraction <= 0.6 &
                  data$MEP_Fraction >= 0.4,]
datamixed <- subdata[subdata$original.outcome == 'm',]
ggplot(subdata, aes(x = uArea, y = Components)) + geom_point()
ggplot(datamixed, aes(x = Components)) + geom_histogram(binwidth = 1) + lims(x = c(0, 15))
ggplot(subdata, aes(x = Components)) + geom_histogram(binwidth = 1)+ lims(x = c(0, 15))

# Graph GFP and H1047R across strains -----------------------------------------

viruses = c('GFP', 'H1047R')
strains = c('029', '059L', '237', '184', '122L', '240L', '353P')

pdata <- data_subset_summary[data_subset_summary$virus %in% viruses &
                               data_subset_summary$strain %in% strains &
                               data_subset_summary$timepoint == 7 &
                               data_subset_summary$ECM == "Matrigel" &
                               data_subset_summary$drug == "",]
pdata

p <- ggplot(pdata, aes(x=strain, y=mean, fill=virus)) +
    geom_errorbar(aes(ymin=mean-stderr, ymax=mean+stderr), width=.2,
                  position=position_dodge(.9), size=1.5)

p <- p + theme_minimal(base_size=15) + ylim(0,1) +
  theme(panel.grid.minor=element_blank(),
        panel.grid.major=element_blank()) +
  geom_bar(stat="identity", color="black",
           position=position_dodge(), size=1.5) +
  scale_fill_manual(values=c_spectral[1:length(unique(pdata$virus))], name='LEP Transgene: ') +
  labs(title="LEP Organization with Control MEP",
       x = "Specimen ID", y = "Fraction Correct")

p

# Inhibitors -----------------------------------------------------------------

viruses = c('GFP', 'H1047R')
strains = '240L'
drugs = c('TFA', 'copanlisib', 'DMSO', 'MK2206', 'afuresertib', 'EHop016',
          'TFA2C', 'copanlisib2T', 'DMSO2M', 'MK22062D', 'afuresertib2D')

pdata <- data_subset_summary[data_subset_summary$virus %in% viruses &
                               data_subset_summary$strain %in% strains &
                               data_subset_summary$timepoint == 7 &
                               data_subset_summary$ECM == "Matrigel" &
                               data_subset_summary$drug %in% drugs,]
pdata

p <- ggplot(pdata, aes(x=drug, y=mean, fill=virus)) +
  geom_errorbar(aes(ymin=mean-stderr, ymax=mean+stderr), width=.2,
                position=position_dodge(.9), size=1.5)

p <- p + theme_minimal(base_size=15) + ylim(0,1) +
  theme(panel.grid.minor=element_blank(),
        panel.grid.major=element_blank()) +
  geom_bar(stat="identity", color="black",
           position=position_dodge(), size=1.5) +
  scale_fill_manual(values=c_spectral[1:length(unique(pdata$virus))], name='LEP Transgene: ') +
  labs(title="LEP Organization with Control MEP",
       x = "Drug", y = "Fraction Correct")
p <- vertxlab(p)
p

# Talinsh -----------------------------------------------------------------

viruses = c('GFP_puro', 'GFP_TLN1sh1puro', 'H1047R_puro', 'H1047R_TLN1sh1puro')
strains = '240L'
drugs = c('')

pdata <- data_subset_summary[data_subset_summary$virus %in% viruses &
                               data_subset_summary$strain %in% strains &
                               data_subset_summary$timepoint == 7 &
                               data_subset_summary$ECM == "Matrigel" &
                               data_subset_summary$drug %in% drugs,]
pdata

p <- ggplot(pdata, aes(x=virus, y=mean, fill=virus)) +
  geom_errorbar(aes(ymin=mean-stderr, ymax=mean+stderr), width=.2,
                position=position_dodge(.9), size=1.5)

p <- p + theme_minimal(base_size=15) + ylim(0,1) +
  theme(panel.grid.minor=element_blank(),
        panel.grid.major=element_blank()) +
  geom_bar(stat="identity", color="black",
           position=position_dodge(), size=1.5) +
  scale_fill_manual(values=c_spectral[1:length(unique(pdata$virus))], name='Transgene: ') +
  labs(title="Organization with Control and H1047R",
       x = "Transgene", y = "Fraction Correct")
p <- vertxlab(p)
p

# The weird combos -------------------------------------------------------------

viruses = c('mChL+GFPL', 'mChM+GFPM', 'mChL+H1047RL', 'mChM+H1047RM')
strains = '240L'
drugs = c('')

pdata <- data_subset_summary[data_subset_summary$virus %in% viruses &
                               data_subset_summary$strain %in% strains &
                               data_subset_summary$timepoint == 7 &
                               data_subset_summary$ECM == "Matrigel" &
                               data_subset_summary$drug %in% drugs,]
pdata

p <- ggplot(pdata, aes(x=virus, y=mean, fill=virus)) +
  geom_errorbar(aes(ymin=mean-stderr, ymax=mean+stderr), width=.2,
                position=position_dodge(.9), size=1.5)

p <- p + theme_minimal(base_size=15) + ylim(0,1) +
  theme(panel.grid.minor=element_blank(),
        panel.grid.major=element_blank()) +
  geom_bar(stat="identity", color="black",
           position=position_dodge(), size=1.5) +
  scale_fill_manual(values=c_spectral[1:length(unique(pdata$virus))], name='LEP Transgene: ') +
  labs(title="LEP Organization with Control MEP",
       x = "Transgene", y = "Fraction Correct")
p <- vertxlab(p)
p
