# 2 May 2018
# Jennifer L Hu
# circ_arrange.r
#
# Accesses and rearranges data in particles/ in preparation for plotting.
#
# Each row contains the following columns:
# 	r,i,j,folder,name,outcome,lumen,center,date,timepoint,strain,strainM,strainL
# 	and independent variables:
#		confluence,ECM,drug,virus,FDG,CD10
# Target: sum_data dataframe with columns:
#	date, timepoint, strain, strainL, strainL, count, outcome, [indvars]
#
# Sum rows with same [strains],outcome,[indvars]
# Also sum by date if plot_by_expt, timepoint if !no_timepoints
# TODO: add section for collapsing all outcomes to three

#  ---------------  accessing and subsetting data   ---------------  #

# load particles
temp = list.files(path=dir_particles, pattern="*.csv")
rawdata = read.csv(file.path(dir_particles,temp[1]), as.is=TRUE, stringsAsFactors=TRUE)
# add expt date (as.Date can get it from expt string)
rawdata$date <- as.Date(temp[1])
# TODO: if length temp > 1, add others

data <- rawdata
# get strain, cell type, ECM, and timepoint from file
splitfile <- strsplit(rawdata$file, split=" ")
data$strain <- sapply(splitfile,extract_strain)
data$celltype <- sapply(splitfile,extract_type)
data$ECM <- sapply(splitfile,extract_ECM)
data$time <- sapply(splitfile,extract_time)

all_strains <- unique(data$strain)
# turn NA into empty string
data[is.na(data)] <- ''
# don't edit the original dataframe
sub_data <- data
# throw away extra metadata columns
sub_data[,c('i','j','folder','imgtype','lumen','center')] <- NULL
# turn date strings into date (type). Sometimes Excel does weird things to it.
sub_data$date <- as.Date(sub_data$date, "%Y-%m-%d")
# subset the data by date(s) and strain(s) of interest
if (all(include_mixed_strains)) {
  strains_rows <- (sub_data$strain %in% strains | sub_data$strainM %in% strains | sub_data$strainL %in% strains)
} else if (include_mixed_strains[1]) {
  strains_rows <- (sub_data$strain %in% strains)
  if (include_mixed_strains[2]) {
    strains_rows <- strains_rows | (sub_data$strainM %in% strains)
  } else if (include_mixed_strains[3]) {
    strains_rows <- strains_rows | (sub_data$strainL %in% strains)
  }
} else if (include_mixed_strains[2]) {
  strains_rows <- (sub_data$strainM %in% strains) & !(sub_data$strain %in% strains)
  if (include_mixed_strains[3])
    strains_rows <- strains_rows | ((sub_data$strainL %in% strains) & !(sub_data$strain %in% strains))
} else {
  strains_rows <- (sub_data$strainL %in% strains) & !(sub_data$strain %in% strains)
}
sub_data <- sub_data[(sub_data$date > earliest_expt) & strains_rows,]
sub_data <- sub_data[(sub_data$date %in% dates),]
stopifnot(nrow(sub_data) > 0)

# subset data by independent variable to plot. if empty, use defaults in all conditions
iind = match(indvar_to_plot, indvars)
if (is.na(iind))
	iind = 0
# initialize logical indexing
indvar_rows <- vector(mode="logical",length = nrow(sub_data))
default_rows <- !(vector(mode="logical",length = nrow(sub_data)))
for (i in 1:length(indvars)) {
  # rename values in sub_data
  if (indvars[i] %in% indvars_to_rename) {
    sub_data[indvars[i]] <- switch(indvars[i],
          confluence = sapply(sub_data$confluence,name_confluence),
          ECM = sapply(sub_data$ECM,name_ECM),
          CD10 = sapply(sub_data$CD10,name_CD10),
          FDG = sapply(sub_data$FDG,name_FDG)
    )
  }
  # for indvars we are not using,
	if (i != iind) {
		# include only rows whose value for indvar == indvar_default
		default_rows <- default_rows & (sub_data[indvars[i]] == indvar_defaults[i])
	} else { # independent variable to plot
	  if (exclude_default) {
  	  nondefault_rows <- (sub_data[indvars[i]] != indvar_defaults[i])
  	  # valid dates 
  	  indvar_dates <- unique(sub_data[nondefault_rows,"date"])
  	  # strains from those dates
  	  indvar_strains <- unique(sub_data[(sub_data$date %in% indvar_dates) & nondefault_rows,"strain"])
  	  # goal: end up with all dates/strains that contain a non-default value for an indvar
  	  indvar_rows <- indvar_rows | (sub_data$strain %in% indvar_strains & sub_data$date %in% indvar_dates)
	  }
	}
}
if (exclude_default) {
  # only include non-default
  sub_data <- sub_data[indvar_rows & default_rows,]
} else {
  sub_data <- sub_data[default_rows,]
}
stopifnot(nrow(sub_data) > 0)

# if combining days, edit timepoint column
if (combine_day) {
	result <- sapply(sub_data$timepoint,tp_to_day)
	sub_data$timepoint <- result
	# subset by timepoint
	if (length(sub_times) < length(result)) {
		sub_data <- sub_data[sub_data$timepoint %in% sub_times,]
	}
}
# turn all timepoints to same
if (no_timepoints) {
	sub_data$timepoint <- 100
}
# treat timepoints as categorical variables so they plot side-by-side
sub_data$timepoint <- factor(sub_data$timepoint)
# unabbreviate everything in the outcome column to match strings in outcomes
result <- sapply(sub_data$outcome,out_to_outcome)
if (combine_successes || !(use_all_outcomes)) {
  # use reduce_outcome function from reduce_plot_outcomes.r
  result <- sapply(result,reduce_outcome)
}

sub_data$outcome <- factor(result,levels=outcomes)

# this line orders strains by strain_order but don't use it if there are more strains than in strain_order
if (all(unique(sub_data$strain) %in% strain_order))
  sub_data$strain <- factor(sub_data$strain, levels=strain_order)
# same for viruses
if (all(unique(sub_data$virus) %in% virus_order))
  sub_data$virus <- factor(sub_data$virus, levels=virus_order)

#  ---------------  aggregation and counting   ---------------  #

# count technical replicates (dates) for each time,strain,indvars
date_data <- sub_data[, c("r", "date", "timepoint", "strain", "strainM", "strainL", indvars)]
# http://www.dummies.com/programming/r/how-to-use-the-formula-interface-in-r/
# collapse all rows with same date
date_data <- aggregate(r ~ date+timepoint+strain+strainM+strainL+
	confluence+ECM+drug+virus+FDG+CD10, date_data, length)
# overwrite date column of length of aggregate
replicates <- aggregate(date ~ timepoint+strain+strainM+strainL+
	confluence+ECM+drug+virus+FDG+CD10, date_data, length)
# change column name from date to technical
names(replicates)[names(replicates) == "date"] <- "technical"

if (plot_by_expt) {
	# count outcomes grouped by date,time,strain,indvars. arbitrary "r" as col to overwrite
	sum_data <- aggregate(r ~ date+timepoint+strain+strainM+strainL+outcome+
		confluence+ECM+drug+virus+FDG+CD10, sub_data, length)
	# change column name from name to count
	names(sum_data)[names(sum_data) == "r"] <- "count"
} else {
	# count outcomes grouped by time,strain,indvars, overwriting "date" column
	sum_data <- aggregate(date ~ timepoint+strain+strainM+strainL+outcome+
		confluence+ECM+drug+virus+FDG+CD10, sub_data, length)
	# change column name from name to count
	names(sum_data)[names(sum_data) == "date"] <- "count"
}

# store biological replicates
replicates <- merge(replicates, aggregate(count ~ timepoint+strain+strainM+strainL+
	confluence+ECM+drug+virus+FDG+CD10, sum_data, sum),
	by = c("timepoint", "strain", "strainM", "strainL", indvars))
# change column name from count to biological
names(replicates)[names(replicates) == "count"] <- "biological"
# sort on strain and then timepoint, then renumber the rows so columns will be returned in that order
replicates <- replicates[with(replicates, order(strainM,strainL,timepoint)),]
rownames(replicates) <- 1:nrow(replicates)
if (is.na(indvar_to_plot)) {
  print(replicates[, c("timepoint","strain","biological","technical")])
} else
  print(replicates[, c("timepoint","strain",indvar_to_plot,"biological","technical")])