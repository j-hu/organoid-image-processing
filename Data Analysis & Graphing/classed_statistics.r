# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Statistics

# to properly do binomial regression response variable needs to be 0 or 1
# remove NA values
binom_data <- sub_data[sub_data$timepoint == 2 & !is.na(sub_data$total), 
	c("total","fraction","age","agegroup","site","virus","drug")]
binom_data[is.na(binom_data)] <- ''
binom_data$successes <- round(binom_data$fraction * binom_data$total)
binom_data$failures <- round((1-binom_data$fraction) * binom_data$total)

# "de-aggregate" the data
deagg_success_data <- binom_data[rep(1:nrow(binom_data), binom_data$successes),
	c("age","agegroup","site","virus","drug")]
deagg_success_data$outcome <- 1
deagg_failure_data <- binom_data[rep(1:nrow(binom_data), binom_data$failures),
	c("age","agegroup","site","virus","drug")]
deagg_failure_data$outcome <- 0
# recombine de-aggregated data
binom_data <- rbind(deagg_failure_data,deagg_success_data)
# ensure vars are factors or you will get error about contrasts
binom_data$drug <- factor(binom_data$drug)

# subset data for age analysis
ages_data <- binom_data[binom_data$virus == "",]
ages_data$virus <- NULL
ages_data$drug <- NULL

# logistic regression on some variables
model <- glm(formula = outcome ~ ., family = binomial(link = "logit"), 
    data = ages_data)
summary(model)