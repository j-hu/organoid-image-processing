source('constants.r')

exptdir <- file.path('/Users','jhu','Box','Gartnerlab Data',
                     'Individual Folders','Jennifer Hu','Data',
                     'Timelapse Data','2019-06-14')
graphdir <- file.path(exptdir, 'Graphs')

data <- read.csv(file.path(exptdir,'Data','quantifiedt.csv'), stringsAsFactors = FALSE)
# dump any where MEP_Fraction is 0 or 1
data <- data[data$MEP_Fraction > 0 & data$MEP_Fraction < 1,]
# dump data where area is way too large
data <- data[data$Pixels < 30000,]

data$ID <- unlist(lapply(data$imfilename, FUN = function(x) {
  strsplit(x, '_')[[1]][1]
}))
data$Type <- unlist(lapply(data$ID, FUN = function(x) {
  strsplit(x, ' ')[[1]][1]
}))
data$hr <- data$frame * 20/60 + 1
data$outcome <- as.factor(unlist(lapply(data$Edge_MEP_Score, FUN = function(x) {
  if (x > 0.7) { return('correct') } else { return('incorrect') }
})))
data$Edge_LEP_Score <- 1-data$Edge_MEP_Score

# remove bad data (visual inspection)
data <- data[!(data$ID %in% c('GFP P9', 'GFP P20',
                              'PIK3CA P5', 'PIK3CA P9', 'PIK3CA P10', 'PIK3CA P17',
                              'PIK3CA P15', 'PIK3CA P20')),]
data[(data$ID == 'PIK3CA P13' & data$frame < 44), 'ID'] <- 'PIK3CA P13a'
data[(data$ID == 'PIK3CA P3' & data$frame < 104), 'ID'] <- 'PIK3CA P3a'
data[(data$ID == 'PIK3CA P2' & data$frame < 104), 'ID'] <- 'PIK3CA P2a'
data[(data$ID == 'PIK3CA P2a' & data$frame > 43), 'ID'] <- 'PIK3CA P2b'

## plotting --------------------------------------------------------------------

ggplot(data, aes(x = Outer_MEP_Score, y = Edge_MEP_Score)) +
  geom_point(alpha = 0.5) + facet_grid(. ~ Type)
ggplot(data, aes(x = hr, y = Outer_MEP_Score, group = ID, color = ID)) +
  geom_line(alpha = 0.5, size = 1.5) + facet_grid(. ~ Type) + guides(color = FALSE) +
  labs(title = 'Structure change over time', x = 'Time (hr)')
ggplot(data, aes(x = hr, y = Pixels*MEP_Fraction, group = ID, color = ID)) +
  geom_smooth(alpha = 0.5, size = 2) + facet_grid(. ~ Type) + guides(color = FALSE) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'MEP Area') + lims(y = c(0,30000))
ggplot(data, aes(x = hr, y = Pixels*(1-MEP_Fraction), group = ID, color = ID)) +
  geom_smooth(alpha = 0.5, size = 2) + facet_grid(. ~ Type) + guides(color = FALSE) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'LEP Area') + lims(y = c(0,30000))
ggplot(data, aes(x = hr, y = Pixels, group = ID, color = ID)) +
  geom_smooth(alpha = 0.5, size = 2) + facet_grid(. ~ Type) + guides(color = FALSE) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Area') + lims(y = c(0,30000))
ggplot(data[data$Type == 'PIK3CA', ], aes(x = hr, y = Pixels, group = ID, color = ID)) +
  geom_smooth(alpha = 0.5, size = 2) + lims(y = c(0,30000)) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Area')
lm(MEP_Fraction ~ hr, data[data$Type == "GFP",])


p <- ggplot(data, aes(x = hr, y = MEP_Fraction, group = ID)) +
  geom_line(alpha = 0.3) + facet_grid(. ~ Type) + guides(color = FALSE) +
  lims(y = c(0,1)) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Total MEP Fraction')
save_pdf(p, filename = file.path(graphdir, 'all timelapse MEP fraction'),
         w = 7, h = 4)
p <- ggplot(data, aes(x = hr, y = Outer_MEP_Score, group = ID)) +
  geom_line(alpha = 0.3) + facet_grid(. ~ Type) + guides(color = FALSE) +
  lims(y = c(0,1)) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Outer MEP Score')
save_pdf(p, filename = file.path(graphdir, 'all timelapse outer MEP score'),
         w = 7, h = 4)
p <- ggplot(data, aes(x = hr, y = Outer_MEP_Fraction, group = ID)) +
  geom_line(alpha = 0.3) + facet_grid(. ~ Type) + guides(color = FALSE) +
  lims(y = c(0,1)) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Outer MEP Fraction')
save_pdf(p, filename = file.path(graphdir, 'all timelapse outer MEP fraction'),
         w = 7, h = 4)

# subset data to only MEP Fractions
subdata <- data[data$MEP_Fraction >= 0.4 & data$MEP_Fraction <= 0.6,]

p <- ggplot(subdata, aes(x = hr, y = MEP_Fraction, group = ID)) +
  geom_line(alpha = 0.3) + facet_grid(. ~ Type) + guides(color = FALSE) +
  lims(y = c(0,1)) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Total MEP Fraction')
save_pdf(p, filename = file.path(graphdir, 'timelapse MEP fraction'),
         w = 7, h = 4)
p <- ggplot(subdata, aes(x = hr, y = Edge_MEP_Score, color = ID)) +
  geom_line(alpha = 0.3, size = 1.5) + facet_grid(. ~ Type) + guides(color = FALSE) +
  lims(y = c(0,1)) + geom_point(size = 2, alpha = 0.2) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Edge MEP Score')
save_pdf(p, filename = file.path(graphdir, 'timelapse edge MEP score'),
         w = 7, h = 4)
p <- ggplot(subdata, aes(x = hr, y = Outer_MEP_Score, color = ID)) +
  geom_smooth(alpha = 0.3) + facet_grid(. ~ Type) + guides(color = FALSE) +
  lims(y = c(0,1)) +
  labs(title = 'Structure change over time', x = 'Time (hr)', y = 'Outer MEP Score')
save_pdf(p, filename = file.path(graphdir, 'timelapse outer MEP score smooth'),
         w = 7, h = 4)

p <- ggplot(subdata, aes(x = hr, fill = outcome)) +
  geom_density(position = "fill", color = 'white') + facet_grid(. ~ Type) +
  labs(x = 'Time (hr)', title = 'Fraction organoids with > 70% MEP at edge')
save_pdf(p, filename = file.path(graphdir, 'timelapse edgeMEP70+'),
         w = 7, h = 4)

steadydata <- subdata[subdata$hr >= 40,]
# make a histogram of boundary LEP
p <- ggplot(steadydata, aes(x = Edge_LEP_Score, color = Type, fill = Type)) +
  geom_density(size = 1.5, alpha = 0.1) + lims(x = c(0,1)) +
  labs(x = 'Fraction Edge LEP', title = 'Probability density after 40 hr')
save_pdf(p, filename = file.path(graphdir, 'density edge LEP 40hr+'),
         w = 7, h = 4)
p <- ggplot(steadydata, aes(x = Type, y = Edge_LEP_Score)) +
  geom_violin(alpha = 0.7) + lims(y = c(0,1)) +
  labs(y = 'Fraction Edge LEP', title = 'Tissue structure distribution after 40 hr')

# Density estimation -----------------------------------------------------------
# The algorithm used in density.default disperses the mass of the empirical
# distribution function over a regular grid of at least 512 points and then uses
# the fast Fourier transform to convolve this approximation with a discretized
# version of the kernel and then uses linear approximation to evaluate the
# density at the specified points.
densityGFP <- density(steadydata[steadydata$Type == 'GFP', 'Edge_LEP_Score'],
                      from = 0, to = 1)
densityPIK <- density(steadydata[steadydata$Type == 'PIK3CA', 'Edge_LEP_Score'],
                      from = 0, to = 1)
# generate probability density dataframe
pdf <- data.frame(EdgeLEP = c(densityGFP[['x']], densityPIK[['x']]),
                  Density = c(densityGFP[['y']], densityPIK[['y']]),
                  Type = c(rep('GFP', 512), rep('PIK3CA', 512)))
p <- ggplot(pdf, aes(x = EdgeLEP, y = Density, color = Type)) + geom_point()
p

names(pdf) <- c('EdgeMEP','Type','Count')
nGFP <- sum(pdf[pdf$Type == 'GFP', 'Count'])
nPIK <- sum(pdf[pdf$Type == 'PIK3CA', 'Count'])
pdf$Density <- NA
for (i in 1:nrow(pdf)) {
  if (pdf[i, 'Type'] == 'GFP') {
    pdf[i,'Density'] <- pdf[i,'Count']/nGFP
  } else {
    pdf[i,'Density'] <- pdf[i,'Count']/nPIK
  }
}
# save this information to a csv file to try non-linear model fitting in MATLAB
write.csv(pdf[,c('Type','EdgeLEP','Density')], file.path(datadir, 'Results', 'timelapse201906_metrics.csv'))
# after running fitBoltzmann in MATLAB
nlm <- read.csv(file.path(datadir, 'Results', 'timelapse201906_fitnlm.csv'))
# compare results
p <- ggplot(nlm, aes(x = EdgeLEP, y = Density, color = Type)) +
  geom_line(size = 1, alpha = 0.8) + lims(x = c(0,1)) +
  geom_line(data = pdf, size = 1.5, alpha = 0.8) + facet_grid(. ~ Type) +
  labs(x = 'Fraction Edge LEP', title = 'Fitted Boltzmann distributions')
p
save_pdf(p, filename = file.path(graphdir, 'density edge LEP fitnlm'),
         w = 7, h = 4)
