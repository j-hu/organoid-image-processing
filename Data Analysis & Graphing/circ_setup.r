library(gridExtra)
library(gapminder)
library(ggplot2)
library(stringr)
library(reshape2)
library(fmsb)
library(xtable)
source('multiplot.r')

## CONSTANTS
dir_particles = "C:/Users/jhu/Documents/Box Sync/Gartnerlab Data/Individual Folders/Jennifer Hu/Analysis/Datafiles/Organoid Circularity/particles/"

# 2 May 2018
# Jennifer L Hu
# circ_setup.r
#
# Contains hard-coded parameters for circ_plot.r. Runs prior to plotting.
#
# Controls:
#	which dates, conditions, and strains are included
#	which timepoints to include (days, sub_times)
#	which of indvars to plot (indvar_to_plot)
#	whether to combine timepoints in days (combine_day)
#	whether to have timepoints at all (no_timepoints)
# 	whether to separate dates (plot_by_expt)
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

combine_day = TRUE                    # combine data by day? *statistics do not work if FALSE
use_all_outcomes = TRUE               # 8 categories instead of 4
combine_successes = TRUE             # combine successes (if use_all_outcomes TRUE, adds borderline category)
source("reduce_plot_outcomes.r")
sub_times = c(2)                      # what days should be displayed?
no_timepoints = FALSE                 # plot all timepoints combined
plot_by_expt = FALSE                   # separate plots by experiment?
earliest_expt = as.Date("2018-01-01") # earliest date of experiment to include

do_statistics = FALSE
# average ratios from each experiment (TRUE), or combine organoids from all data (FALSE)?
# TRUE: gives equal weight to each experiment no matter how many organoids counted
# TODO: make this work for plotting, not just statistics
stratify_by_expt = FALSE

strains <- all_indiv_strains
indvar_to_plot <- "virus"              # choose one of "confluence","ECM","drug","virus","FDG","CD10",NA
exclude_default <- TRUE           # if using indvar, exclude dates for which only default condition is present


# experiment dates
dates <- all_dates
# dates <- as.Date(c('2018-04-11'))

### make sure settings are consistent
if (length(dates) == 1) {
  plot_by_expt = FALSE
  stratify_by_expt = FALSE
}
if (is.na(indvar_to_plot))
  exclude_default <- FALSE
stopifnot(any(include_mixed_strains))
# if doing heterochronic only, remove 240L from strains or you'll be swamped
if (!include_mixed_strains[1]) {
  strains <- strains[strains != "240L"]
}
unmixed_only <- all(include_mixed_strains == c(TRUE,FALSE,FALSE))

# functions for getting info out of file name
extract_string <- function(splitname,s) {
	options = switch(s,
		strain = c("240L"),
		celltype = c("LEP","MEP"),
		ECM = c("3COL","3COL1MG","3COL60m","MG"),
		time = c("24h")
	)
	l = length(splitname)
	# get rid of last after hyphen, which is the file number
	s = strsplit(splitname[l],split="-")
	splitname[l] = s[1]
	# iterate through elements of splitname
	for i=1:l {
		if splitname[i] %in% options {
			# return the correct option
			return(option[sapply(options,{function (x) grepl(x,splitname[i])})])
		}
		# else do nothing
	}
}